package com.example.homework6

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loaddata()
        init()
    }
    private fun init(){
        save.setOnClickListener {
            saveData()
        }

    }
    private fun saveData(){
        val email = email.text.toString()
        val name = firstName.text.toString()
        val surname = lastName.text.toString()
        val aGe = age1234.text.toString()
        val address = address.text.toString()
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply(){
            putString("EMAIL",email)
            putString("NAME",name)
            putString("LASTNAME",surname)
            putString("AGE",aGe)
            putString("ADDRESS",address)
        }.apply()
        Toast.makeText(this,"Data Saved", Toast.LENGTH_SHORT).show()
    }
     private fun loaddata(){
        val sharedPreferences = getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val savedEmail = sharedPreferences.getString("EMAIL","type email")
        val savedName = sharedPreferences.getString("NAME","type name")
        val savedSurname = sharedPreferences.getString("LASTNAME","type last name")
        val savedAge = sharedPreferences.getString("AGE","type age")
        val savedAddress = sharedPreferences.getString("ADDRESS","type address")

         email.setText(savedEmail)
         firstName.setText(savedName)
        lastName.setText(savedSurname)
        age1234.setText(savedAge)
        address.setText(savedAddress)
    }

}